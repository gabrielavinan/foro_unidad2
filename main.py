# -*- coding: utf-8 -*-

import sys
import pygame
import random

from nave import *
from asteroide import *
from score import *
from boton import *
from cursor import *
from highscore import *

# Colores
AZUL = (100, 100, 205) # Se modificó el color de la letra
NEGRO = (0, 0, 0)

# Tamaño de la ventana del juego
ANCHO = 600
ALTO = 680

# Lista de asteriodes
ASTERIODES = []


def newText(texto, superficie, posicion=(0, 0), color=NEGRO, length=12):
    """
    Metodo generico para crear texto.
    """
    fuente = pygame.font.SysFont("comicsansms", length)
    text = fuente.render(texto, True, color)
    superficie.blit(text, posicion)


def pause(ventana, en_pausa):
    """
    Menu de Pausa del juego.
    """
    fps = pygame.time.Clock()

    while en_pausa:
        for evento in pygame.event.get():
            # Click en la "X" de la ventana
            if evento.type == pygame.QUIT:
                pygame.quit()
                sys.exit()

            if evento.type == pygame.KEYDOWN:
                    # Despausa el juego
                    if evento.key == pygame.K_SPACE:
                        en_pausa = False

        newText("Paused", ventana, (230, 300), AZUL, 50)
        
        pygame.display.update()
        
        fps.tick(15)


def gameMenu(ventana, sound_click):
    """
    Menu principal del juego.
    """
    cursor = Cursor()

    btn_play = Boton("images/play.png", 228, 250) #Se modifició el diseño de imágen y las posiciones 
    btn_help = Boton("images/help.jpg", 228, 350) #Se modifició el diseño de imágen y las posiciones  
    btn_quit = Boton("images/quit.png", 228, 450) #Se modifició el diseño de imágen y las posiciones 

    fps = pygame.time.Clock()

    fondo = pygame.image.load("images/fondo2.jpg") #Se modificó el diseño del segundo fondo
    logo = pygame.image.load("images/logo.png") #Se modificó el diseño del logo 

    click_button = False

    while not click_button:
        for evento in pygame.event.get():
            # Click en la "X" de la ventana
            if evento.type == pygame.QUIT:
                pygame.quit()
                sys.exit()

            # Click del Mouse
            if evento.type == pygame.MOUSEBUTTONDOWN:
                # Click en el boton de "Play Game"
                if cursor.colliderect(btn_play.rect):
                    sound_click.play() # Sonido de Click
                    click_button = True
                # Click en el boton de "Help"
                if cursor.colliderect(btn_help.rect):
                    sound_click.play() # Sonido de Click
                    helpMenu(ventana, sound_click)
                # Click en el boton de "Quit"
                if cursor.colliderect(btn_quit.rect):
                    sound_click.play() # Sonido de Click
                    pygame.quit()
                    sys.exit()

        ventana.blit(fondo, (0, 0))

        cursor.update()

        # Nombre del juego
        ventana.blit(logo, (100, 100))

        btn_play.dibujar(ventana)
        btn_help.dibujar(ventana)
        btn_quit.dibujar(ventana)

        # Muestro el High Score local
        newText("Puntuación más alta: " + str(getHighScore()), ventana, (40, 550), AZUL, 50) #Se modificó el texto y la posición

        pygame.display.update()
        fps.tick(15)


def helpMenu(ventana, sound_click):
    """
    Menu de ayuda del juego.
    """
    cursor = Cursor()

    fondo = pygame.image.load("images/fondo2.jpg") #Se modifició el diseño de imágen
    tecla_a = pygame.image.load("images/letra a.png") #Se modifició de tecla direccional izquierda a tecla (a) 
    tecla_l = pygame.image.load("images/letra l.png") #Se modifició de tecla direccional derecha a tecla (l) 
    tecla_space = pygame.image.load("images/letra p.png") #Se modifició el diseño de imágen

    btn_back = Boton("images/back.jpg", 228, 600) #Se modifició el diseño de imágen

    fps = pygame.time.Clock()

    is_back = False

    while not is_back:
        for evento in pygame.event.get():
            # Click en la "X" de la ventana
            if evento.type == pygame.QUIT:
                pygame.quit()
                sys.exit()

            # Click del Mouse
            if evento.type == pygame.MOUSEBUTTONDOWN:
                # Click en el boton de "Play Game"
                if cursor.colliderect(btn_back.rect):
                    sound_click.play() # Sonido de Click
                    is_back = True

        ventana.blit(fondo, (0, 0))
        
        cursor.update()

        newText("Ayuda", ventana, (210, 50), AZUL, 100) #Se modificó el texto 

        ventana.blit(tecla_a, (100, 200))
        newText("Mover hacia la izquierda", ventana, (200, 200), AZUL, 30) #Se modificó el texto y movimiento

        ventana.blit(tecla_l, (100, 300))
        newText("Mover hacia la derecha", ventana, (200, 300), AZUL, 30) #Se modificó el texto y movimiento

        ventana.blit(tecla_space, (100, 400))
        newText("Pausa", ventana, (200, 400), AZUL, 30)

        btn_back.dibujar(ventana)

        pygame.display.update()
        
        fps.tick(15)


def gameLoop(ventana, sound_click):
    """
    Loop principal del juego.
    """
    # Fondo del juego
    fondo = pygame.image.load("images/fondo.jpg") #Se modificó el diseño del fondo de la ventana principal
    
    # FPS en que se movera el juego
    fps = pygame.time.Clock()

    en_juego = True
    en_pausa = False

    cursor = Cursor()
    nave = Nave(ANCHO)
    score = Score(0, 0, AZUL)

    btn_play_again = Boton("images/play_again.jpg", 170, 250) #Se cambió diseño y posicipon del botón 
    btn_quit = Boton("images/quit.png", 235, 320) #Se cambió diseño  de imágen 

    cargarAsteriodes(ASTERIODES)
    moverAsteroides(ASTERIODES)

    asteroide = elegirAsteriode(ASTERIODES)

    is_highscore = False
    is_destroy = True # Para que solo reproduzca el sonido una sola vez

    while True:
        for evento in pygame.event.get():
            # Click en la "X" de la ventana
            if evento.type == pygame.QUIT:
                pygame.quit()
                sys.exit()

            if en_juego:
                if evento.type == pygame.KEYDOWN:
                    # Pausa el juego
                    if evento.key == pygame.K_SPACE: #Se modificó el uso de teclas de tecla (p) a  tecla (space)
                        en_pausa = True
                        pause(ventana, en_pausa)
                    # Mover nave a la izquierda
                    if evento.key == pygame.K_a: #Se modificó el uso de teclas de tecla (direccional izquierda) a  tecla (a)
                        nave.moverIzquierda()
                    # Mover nave a la derecha
                    elif evento.key == pygame.K_l: #Se modificó el uso de teclas de tecla (direccional derecha) a  tecla (l)
                        nave.moverDerecha()

            # Click del Mouse
            if evento.type == pygame.MOUSEBUTTONDOWN:
                # Click en el boton de "Play Again"
                if cursor.colliderect(btn_play_again.rect):
                    sound_click.play() # Sonido de Click
                    detenerAsteriodes(ASTERIODES)
                    gameLoop(ventana, sound_click)
                # Click en el boton de "Quit"
                if cursor.colliderect(btn_quit.rect):
                    sound_click.play() # Sonido de Click
                    pygame.quit()
                    sys.exit()
        # Cargo el fondo
        ventana.blit(fondo, (0, 0))
        
        # Dibujo la nave
        nave.dibujar(ventana)
        # Dibujo el puntaje
        score.dibujar(ventana)

        # Si estoy jugando, dibujo los asteroides y los muevo
        if en_juego:
            asteroide.dibujar(ventana)
            asteroide.mover()

        # Si choco con un asteroide
        if asteroide.rect.colliderect(nave.rect):
            nave.destruccion()
            nave.sonidoDestruccion(is_destroy)
            en_juego = False
            is_destroy = False # Para que solo reproduzca el sonido una sola vez
            newText("GAME OVER", ventana, (110, 100), AZUL, 50) #Se cambió la posiicón del mensaje Game over
            btn_play_again.dibujar(ventana)
            btn_quit.dibujar(ventana)
            
            # Obtengo el puntaje obtenido
            mi_puntaje = score.getPuntaje() # Tipo int

            # Seteo el nuevo puntaje mas alto
            if mi_puntaje > getHighScore():
                is_highscore = True
                setHighScore(score.getPuntaje())

            # Si obtuve un nuevo High Score lo muestro
            if is_highscore:
                newText("New High Score: " + str(mi_puntaje), ventana, (170, 390), AZUL, 50)

        # Si el asteroide no choca con la nave
        if asteroide.rect.top > ANCHO + 100:
            if en_juego:
                score.aumentar()

            asteroide.rect.top = 10 # Coloco el asteriode en su posicion original
            moverAsteroides(ASTERIODES)
            asteroide = elegirAsteriode(ASTERIODES)



        cursor.update()

        fps.tick(20) # 20 FPS

        pygame.display.update()


def main():
    pygame.init() # Inicializacion del los modulos de Pygame
    
    # Creacion Ventana
    ventana = pygame.display.set_mode((ANCHO, ALTO))
    
    # Nombre de la ventana
    pygame.display.set_caption("Space Ship")
    
    # Icono del juego
    icon = pygame.image.load("images/icon.png") 
    pygame.display.set_icon(icon)

    # Musica de fondo


    sound_click = pygame.mixer.Sound("sounds/click.wav") # Sonido de click

    gameMenu(ventana, sound_click)
    gameLoop(ventana, sound_click)
main()
